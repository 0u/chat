package chat

import (
	"io/ioutil"
	"testing"
)

func TestAppend(t *testing.T) {
	room := NewRoom()

	room.append(ioutil.Discard)
	if room.len() != 1 {
		t.Fatalf("want length %d got length %d", 1, room.len())
	}
	room.delete(1)

	if room.len() != 0 {
		t.Fatalf("want length %d got length %d", 0, room.len())
	}
}
