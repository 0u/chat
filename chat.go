package chat

import "io"

// Uesr represents a user connected to the room.
type User interface {
	// A user must also be a ReadWriteCloser.
	io.ReadWriteCloser

	// The users name.
	Name() string
}

// Room represents a chatroom.
type Room struct {
	Users    map[string]User // id -> user
	Action   chan func()     // actions for the daemon to run.
	quit     chan struct{}   // stop the daemon goroutine.
	NewUsers chan User       // new users!
}

// Stop the daemon goroutine.
func (r *Room) Close() {
	close(r.quit)
}

func (r *Room) run() {
	for {
		select {
		case user := <-r.NewUsers:
			r.Users[user.Name()] = user
		case <-r.quit:
			return
		}
	}
}

// Create a new chatroom. (maybe call just new?)
func NewRoom() Room {
	r := Room{
		Users:    make(map[string]User),
		Action:   make(chan func()),
		quit:     make(chan struct{}),
		NewUsers: make(chan User),
	}

	go r.run()
	return r
}
